const slide = document.querySelector('.slide');
const carousel = document.querySelector('.carousel');

const prev = document.querySelector('.prev');
const next = document.querySelector('.next');
const incicatorParent = document.querySelector('.control ul');

var direction;
prev.addEventListener('click', function () {
    if (direction === -1) {
        slide.appendChild(slide.firstElementChild);
        direction = 1;
    }
    carousel.style.justifyContent = 'flex-end';
    slide.style.transform = 'translate(50%)';
});

next.addEventListener('click', function () {
    direction = -1;
    carousel.style.justifyContent = 'flex-start';
    slide.style.transform = 'translate(-50%)';
});

slide.addEventListener('transitionend', function () {
    if (direction === -1) {
        slide.appendChild(slide.firstElementChild);
    } else if (direction === 1) {
        slide.prepend(slide.lastElementChild);
    }

    slide.style.transition = 'none';
    slide.style.transform = 'translate(0)';
    setTimeout(function () {
        slide.style.transition = 'all 0.5s';
    });
})

document.querySelectorAll('.controls li').forEach(function (indicator) {
    indicator.addEventListener('click', function () {
        document.querySelector('.controls .selected').classList.remove('selected')
        if (direction === -1) {
            indicator.classList.add('selected');
            slide.appendChild(slide.firstElementChild);
        } else if (direction === 1) {
            indicator.classList.add('selected');
            slide.prepend(slide.lastElementChild);
        }
    });
});

function updateSelectedIndicator() {
    const currentSlideIndex = Array.from(slide.children).indexOf(slide.firstElementChild);
    const indicators = document.querySelectorAll('.controls ul li');
    indicators.forEach((indicator, index) => {
        if (index === currentSlideIndex) {
            indicator.classList.add('selected');
        } else {
            indicator.classList.remove('selected');
        }
    });
}


//jquery for toggle dropdown menus
$(document).ready(function () {
    //toggle sub-menus
    $(".sub-btn").click(function () {
        $(this).next(".sub-menu").slideToggle();
    });

    //toggle more-menus
    $(".more-btn").click(function () {
        $(this).next(".more-menu").slideToggle();
    });
});

//javascript for the responsive navigation menu
var menu = document.querySelector(".menu");
var menuBtn = document.querySelector(".menu-btn");
var closeBtn = document.querySelector(".close-btn");

menuBtn.addEventListener("click", () => {
    menu.classList.add("active");
});

closeBtn.addEventListener("click", () => {
    menu.classList.remove("active");
});

//javascript for the navigation bar effects on scroll
window.addEventListener("scroll", function () {
    var header = document.querySelector("header");
    header.classList.toggle("sticky", window.scrollY > 0);
});


const form = document.getElementById('form');
const username = document.getElementById('name');
const email = document.getElementById('email');
const message = document.getElementById('message');
const submitBtn = document.getElementById('submitBtn');

form.addEventListener('submit', e => {
    e.preventDefault();
    validateInputs();
});

submitBtn.addEventListener('click', () => {
    if (validateInputs()) {
        form.submit();
    }
});


const setError = (element, message) => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = message;
    inputControl.classList.add('error');
    inputControl.classList.remove('success');
};

const setSuccess = element => {
    const inputControl = element.parentElement;
    const errorDisplay = inputControl.querySelector('.error');

    errorDisplay.innerText = '';
    inputControl.classList.add('success');
    inputControl.classList.remove('error');
};

const isValidEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validateInputs = () => {
    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const messageValue = message.value.trim();
    let isValid = true;

    if (usernameValue === '') {
        setError(username, 'Name is required');
        isValid = false;
    } else {
        setSuccess(username);
    }

    if (emailValue === '') {
        setError(email, 'Email is required');
        isValid = false;
    } else if (!isValidEmail(emailValue)) {
        setError(email, 'Provide a valid email address');
        isValid = false;
    } else {
        setSuccess(email);
    }

    if (messageValue === '') {
        setError(message, 'Message is required');
        isValid = false;
    } else {
        setSuccess(message);
    }

    return isValid;
};

